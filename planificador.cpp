#include <iostream>
#include <iomanip>
#include <stdio.h>
#include "mods/Gantt.h"
#include "mods/ListaProcesos.h"
#include <limits>

#define clrscr() printf("\033[H\033[J");

using namespace std;

void imprimirBienvenida();

int main()
{
    Proceso cpu;  // Variable que determina que proceso esta en CPU en un tiempo dado.
    char opt;

    // Se imprime mensaje de bienvenida.
    imprimirBienvenida();

    do {
        Gantt gantt = crearGanttVacio();  // Variable que representa el diagrama gantt, guardando el historial de ejecuciones.
        ListaProcesos procEntrantes = crearListaProcesos();  // Lista que guarda los procesos ingresados y sirve de cola de listos.
        ListaProcesos procProcesados = crearListaProcesos();  // Lista que guarda los procesos ya finalizados para poder hacer las cuentas.
        int cantProcesos, leerArrivalTime, leerBurstTime;
        bool ready = false;

        do {
            cout << "Por favor ingrese la cantidad total de procesos a ejecutar: ";
            cin >> cantProcesos;

            if (cantProcesos > 0)
                ready = true;

            else {
                cout << "Error: La cantidad total de procesos a ejecutar debe ser mayor a cero." << endl;
                cout << endl;

                // Encontrado en https://stackoverflow.com/a/18400649/3508426
                // Soluciona el problema del loop infinito cuando se espera leer un int, y el usuario
                // ingresa un char.
                cin.clear();
                cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }

        } while ( ! ready);

        for (int i = 0; i < cantProcesos; ++i) {
            ready = false;

            cout << endl;
            do {
                cout << "Proceso " << i + 1 << ":" << endl;
                cout << "Ingrese el tiempo de arribo: ";
                cin >> leerArrivalTime;

                cout << "Ingrese el tiepo de ráfaga: ";
                cin >> leerBurstTime;

                if (leerArrivalTime >= 0 && leerBurstTime > 0)
                {
                    ready = true;
                    agregarProceso(procEntrantes, crearProceso(leerBurstTime, leerArrivalTime, i + 1));
                }

                else {
                    cout << endl;

                    if (leerArrivalTime < 0)
                        cout << "Error: El tiempo de arribo debe ser mayor o igual a cero." << endl;

                    if (leerBurstTime <= 0)
                        cout << "Error: El tiempo de ráfaga debe ser mayor a cero." << endl;

                    cout << "Por favor, ingrese de nuevo los valores para el proceso." << endl;
                    cout << endl;

                    // Encontrado en https://stackoverflow.com/a/18400649/3508426
                    // Soluciona el problema del loop infinito cuando se espera leer un int, y el usuario
                    // ingresa un char.
                    cin.clear();
                    cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                }

            } while( ! ready);
        }

        // while que simula los ciclos de reloj del CPU
        bool done = false;
        int ciclo = 0;

        while ( ! done)
        {
            cpu = obtenerProceso(procEntrantes, ciclo);

            if ( ! esVacioGantt(gantt) && cpu == obtenerUltimoProceso(gantt))
                sumarCicloAProcesoActual(gantt);

            else
                insertarProcesoGantt(gantt, cpu);

            if (cpu != NULL && restarUnCicloReloj(cpu) < 1)
            {
                eliminarNodo(procEntrantes, cpu, ciclo);
                agregarProcesoOrdenado(procProcesados, cpu);
            }

            aumentarTiemposEspera(procEntrantes, cpu, ciclo);

            ciclo++;

            // Si ya no hay procesos en la lista, se corta el bucle.
            if (esVacio(procEntrantes))
                done = true;
        }

        cout << endl << endl;
        cout << endl << endl;
        printGantt(gantt);  // Se imprime el Gantt.

        cout << endl << endl;
        int sumTiempoEspera = 0, sumTiempoRetorno = 0;
        printCalcs(inicioLista(procProcesados), sumTiempoEspera, sumTiempoRetorno);  // Se imprimen los calculos de los procesos

        cout << "El tiempo de espera promedio es: " << fixed << setprecision(2) << (sumTiempoEspera * 1.0) / cantProcesos << endl;
        cout << "El tiempo de retorno promedio es: " << fixed << setprecision(2) << (sumTiempoRetorno * 1.0) / cantProcesos << endl;

        // Preguntamos al usuario si desea continuar.
        cout << endl;
        cout << "¿Desea continuar? (S/N) ";
        cin >> opt;
        cout << endl;
        
    // El programa se ejecuta en loop mientras que el usuario siga indicando que desea continuar usandolo.
    } while (opt == 'S' || opt == 's');

    cout << "¡Muchas gracias por utilizar nuestro programa!" << endl;

    return 0;
}

void imprimirBienvenida()
{
    clrscr();
    cout << "---------------------------------------------------------" << endl;
    cout << "------     Obligatorio 1 – Sistemas Operativos     ------" << endl;
    cout << "------          Tecnólogo en Informática           ------" << endl;
    cout << "------                   Grupo 6                   ------" << endl;
    cout << "------  Planificador de Procesos SJF Expropiativo  ------" << endl;
    cout << "---------------------------------------------------------" << endl;
    cout << endl;
}
