#ifndef GANTTH
#define GANTTH

#include "Proceso.h"

struct _gantt;
typedef struct _gantt* Gantt;

//Crear un Gantt vacío
Gantt crearGanttVacio();

//Insertar procesos al Gantt
void insertarProcesoGantt(Gantt &g, Proceso p);

// Getters
Proceso obtenerUltimoProceso(Gantt g);
// Pre:
// Post: retorna el ultimo proceso que se inserto en el Gantt.

void sumarCicloAProcesoActual(Gantt &g);
// Pre:
// Post: suma uno al tamanio del bloque generado por el proceso actual.

void printGantt(Gantt g);
// Pre:
// Post: Imprime el gantt.

// Predicados
bool esVacioGantt(Gantt g);

#endif
