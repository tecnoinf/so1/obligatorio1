#include "Proceso.h"
#include <stdio.h>

struct _proceso {
    int burstTime;
    int arrivalTime;
    int pid;
    int tiempoFin;
    int tiempoEspera;
};

Proceso crearProceso(int burstTime, int arrivalTime, int pid)
{
    Proceso p = new _proceso;

    p->burstTime = burstTime;
    p->arrivalTime = arrivalTime;
    p->pid = pid;
    p->tiempoFin = -1;
    p->tiempoEspera = 0;

    return p;
}


void restarBursTime(Proceso &p)
{

	p->burstTime--;

}

void aumentarTiempoEspera(Proceso p)
{
    p->tiempoEspera++;
}

int getBurstTime(Proceso p)
{

	return p->burstTime;

}

int getArrivalTime(Proceso p)
{

	return p->arrivalTime;

}

int getTiempoFin(Proceso p)
{
    return p->tiempoFin;
}

int getTiempoEspera(Proceso p)
{
    return p->tiempoEspera;
}

void setTiempoFin(Proceso p, int cicloFin)
{
    p->tiempoFin = cicloFin + 1;
}

int restarUnCicloReloj(Proceso &p)
{
	p->burstTime--;
	return p->burstTime;
}

int getPid(Proceso p)
{
	return p->pid;
}












