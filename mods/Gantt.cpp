#include "Gantt.h"
#include "Proceso.h"
#include <iostream>
#include <iomanip>
#include <sstream>

using namespace std;

struct _dataGantt {
    Proceso proc;
    int width;
};

struct _gantt {
    struct _dataGantt data;
    struct _gantt *sig;
};

Gantt crearGanttVacio()
{
    return NULL;
}

void insertarProcesoGantt(Gantt &g, Proceso p)
{
    Gantt aux = new _gantt;

    aux->data.proc = p;
    aux->data.width = 0;
    aux->sig = g;

    g = aux;
}

Proceso obtenerUltimoProceso(Gantt g)
{
    return g->data.proc;
}

void sumarCicloAProcesoActual(Gantt &g)
{
    g->data.width++;
}

void printHeaderFooter(Gantt g)
{
    if (g != NULL)
    {
        printHeaderFooter(g->sig);

        cout << setw((g->data.width + 1) * 3) << setfill('-') << "";
        cout << "+";
    }
}

void printContent(Gantt g)
{
    if (g != NULL)
    {
        printContent(g->sig);

        stringstream s;
        if (g->data.proc != NULL)
            s << "P" << getPid(g->data.proc);

        else
            s << "";

        cout << left << setw((g->data.width + 1) * 3) << setfill(' ') << s.str();
        cout << "|";
    }
}

void printNumbers(Gantt g, int &tiempos)
{
    if (g != NULL)
    {   
        printNumbers(g->sig, tiempos);

        cout << left << setw(((g->data.width + 1) * 3) + 1) << setfill(' ') << tiempos;
        tiempos= tiempos+(g->data.width + 1) ;   
    }
}

void printGantt(Gantt g)
{
    // Impresion del header.
    cout << "+";
    printHeaderFooter(g);
    cout << endl;

    // Impresion del contenido.
    cout << "|";
    printContent(g);
    cout << endl;

    // Impresion del footer.
    cout << "+";
    printHeaderFooter(g);
    cout << endl;

    // Impresion de los numeros.
    int tpos=0;
    printNumbers(g, tpos);
    cout << tpos << endl;
}

bool esVacioGantt(Gantt g)
{
    return g == NULL;
}
