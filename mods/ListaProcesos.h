#ifndef LISTAPPROCESOSH
#define LISTAPPROCESOSH

#include "Proceso.h"

struct _cabeceraListaProcesos;
typedef struct _cabeceraListaProcesos* ListaProcesos;

struct _listaProcesos;
typedef struct _listaProcesos* Nodo;

// Constructoras
ListaProcesos crearListaProcesos();

void agregarProceso(ListaProcesos &l, Proceso p);

void agregarProcesoOrdenado(ListaProcesos &l, Proceso p);
// Pre:
// Post: Agrega el proceso 'p' a la lista ordenado por PID.

Nodo inicioLista(ListaProcesos l);
// Pre:
// Post: Retorna el puntero al primer elemento de la lista.

Proceso obtenerProceso(ListaProcesos &l, int cicloActual);
// Pre:
// Post: Retorna el proceso que se debe ejecutar a continuacion,
//       esto es:
//           -> El proceso que tiene menor burstTime
//           -> Su arrivalTime es menor-igual al ciclo de reloj actual

void eliminarNodo(ListaProcesos &l, Proceso p, int cicloActual);
// Pre:
// Post: Elimina el nodo que almacena la direccion de memoria
//       de 'p', sin eliminar la memoria del proceso en si.

void aumentarTiemposEspera(ListaProcesos l, Proceso p, int cicloActual);
// Pre:
// Post: Aumenta el tiempo de espera en 1 a todos los procesos de la
//       lista, excepto al proceso 'p'

// Predicados
bool esVacio(ListaProcesos l);

void printCalcs(Nodo lPrimero, int &sumTiempoEspera, int &sumTiempoRetorno);
// Pre:
// Post: Realiza los calculos en base a los procesos finalizados.

#endif