#include <stdio.h>
#include <iostream>
#include "ListaProcesos.h"

using namespace std;

struct _listaProcesos {
    Proceso proc;
    _listaProcesos *sig;
};

struct _cabeceraListaProcesos {
    Nodo primero;
    Nodo ultimo;
};

ListaProcesos crearListaProcesos()
{
    ListaProcesos l = new _cabeceraListaProcesos;

    l->primero = NULL;
    l->ultimo = NULL;

    return l;
}

void agregarProceso(ListaProcesos &l, Proceso p)
{
    // Creamos el nuevo nodo de la lista.
    Nodo n = new _listaProcesos;

    // Populamos la estructura
    n->proc = p;
    n->sig = NULL;

    if (l->primero == NULL)
        // Si la lista es vacia, hacemos que el primer elemento de la lista
        // sea el nuevo nodo.
        l->primero = n;

    if (l->ultimo != NULL)
        // Si la lista no es vacia, hacemos que el ultimo elemento de esta
        // apunte al nuevo nodo
        l->ultimo->sig = n;

    // El nuevo nodo pasa a ser el ultimo elemento de la lista.
    l->ultimo = n;
}

void agregarProcesoOrdenado(ListaProcesos &l, Proceso p)
{
    int pidAux = getPid(p);
    Nodo lIter = l->primero;
    Nodo lIterAnt = NULL;

    // Creamos el nuevo nodo de la lista.
    Nodo n = new _listaProcesos;

    // Populamos la estructura
    n->proc = p;
    n->sig = NULL;

    // Mientras no se llegue al final de la lista, ni suceda que el proceso que queremos
    // insertar tenga un PID menor al de la posicion de iteracion.
    while (lIter != NULL && pidAux < getPid(lIter->proc))
    {
        // Avanzamos los iteradores.
        lIterAnt = lIter;
        lIter = lIter->sig;
    }

    if (lIter != NULL)
    {
        if (lIterAnt == NULL)
        {
            // Si hay que insertar el proceso al inicio de la lista.
            n->sig = l->primero;
            l->primero = n;
        }
        else
        {
            // Si el proceso se inserta en el medio de la lista.
            n->sig = lIter;
            lIterAnt->sig = n;
        }
    }
    else
    {
        if (lIterAnt == NULL)
        {
            // Si la lista es vacia.
            l->primero = n;
            l->ultimo = n;
        }
        else
        {
            // Si se inserta el elemento al final de la lista.
            lIterAnt->sig = n;
            l->ultimo = n;
        }
    }
}

Nodo inicioLista(ListaProcesos l)
{
    return l->primero;
}

Proceso obtenerProceso(ListaProcesos &l, int cicloActual)
{
    Nodo lAux = l->primero;
    int menorBurstTime = -1;
    int tmpBurstTime;
    Proceso pAux = NULL;

    while (lAux != NULL)
    {
        tmpBurstTime = getBurstTime(lAux->proc);

        if (getArrivalTime(lAux->proc) <= cicloActual && (menorBurstTime == -1 || menorBurstTime > tmpBurstTime))
        {
            menorBurstTime = tmpBurstTime;
            pAux = lAux->proc;
        }

        lAux = lAux->sig;
    }

    return pAux;
}

bool esVacio(ListaProcesos l)
{
    return l->primero == NULL || l->ultimo == NULL;
}

void eliminarNodo(ListaProcesos &l, Proceso p, int cicloActual)
{
    Nodo lAux = NULL;
    Nodo lIter = l->primero;

    while (lIter != NULL && lIter->proc != p)
    {
        lAux = lIter;
        lIter = lIter->sig;
    }

    if (lIter != NULL)
    {
        if (lAux == NULL)
            l->primero =lIter->sig;

        else
            lAux->sig = lIter->sig;

        setTiempoFin(lIter->proc, cicloActual);

        delete lIter;
    }
}

void aumentarTiemposEspera(ListaProcesos l, Proceso p, int cicloActual)
{
    Nodo lAux = l->primero;

    while (lAux != NULL)
    {
        if (lAux->proc != p && getArrivalTime(lAux->proc) <= cicloActual)
        {
            aumentarTiempoEspera(lAux->proc);
        }

        lAux = lAux->sig;
    }
}

void printCalcs(Nodo lPrimero, int &sumTiempoEspera, int &sumTiempoRetorno)
{
    if (lPrimero != NULL)
    {
        printCalcs(lPrimero->sig, sumTiempoEspera, sumTiempoRetorno);

        if (lPrimero->proc != NULL)
        {
            int tEspera = getTiempoEspera(lPrimero->proc);
            int tRetorno = getTiempoFin(lPrimero->proc) - getArrivalTime(lPrimero->proc);

            sumTiempoEspera = sumTiempoEspera + tEspera;
            sumTiempoRetorno = sumTiempoRetorno + tRetorno;

            cout << "Proceso " << getPid(lPrimero->proc) << ":" << endl;
            cout << "    Tiempo de espera: " << tEspera << endl;
            cout << "    Tiempo de retorno: " << tRetorno << endl << endl;
        }
    }
}
