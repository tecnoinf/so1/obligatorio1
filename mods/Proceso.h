#ifndef PROCESOH
#define PROCESOH

struct _proceso;
typedef struct _proceso* Proceso;

// Constructoras
Proceso crearProceso(int burstTime, int arrivalTime, int pid);

// Getters
int getPid(Proceso p);
int getBurstTime(Proceso p);
int getArrivalTime(Proceso p);
int getTiempoFin(Proceso p);
int getTiempoEspera(Proceso p);

// Setters
void setTiempoFin(Proceso p, int cicloFin);
void aumentarTiempoEspera(Proceso p);

int restarUnCicloReloj(Proceso &p);
// Pre: p != null
// Post: Resta 1 al burstTime, y retorna el numero en el que quedo dicho valor.

#endif